========
Overview
========

An example package. Generated with cookiecutter-pylibrary.

Installation
============

::

    pip install crevnet

You can also install the in-development version with::

    pip install git+ssh://git@gitlab/rrxi/crevnet.git@master

Documentation
=============


https://paperswithcode.com/paper/crevnet-conditionally-reversible-video

https://paperswithcode.com/paper/efficient-and-information-preserving-future


Development
===========

To run all the tests run::

    tox

Note, to combine the coverage data from all the tox environments run:

.. list-table::
    :widths: 10 90
    :stub-columns: 1

    - - Windows
      - ::

            set PYTEST_ADDOPTS=--cov-append
            tox

    - - Other
      - ::

            PYTEST_ADDOPTS=--cov-append tox
