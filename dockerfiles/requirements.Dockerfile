ARG DOCKER_BASE_IMAGE_PREFIX
ARG DOCKER_BASE_IMAGE_NAMESPACE=pythonpackagesonalpine
ARG DOCKER_BASE_IMAGE_NAME=basic-python-packages-pre-installed-on-alpine
ARG DOCKER_BASE_IMAGE_TAG=tox-alpine
FROM ${DOCKER_BASE_IMAGE_PREFIX}$DOCKER_BASE_IMAGE_NAMESPACE/$DOCKER_BASE_IMAGE_NAME:$DOCKER_BASE_IMAGE_TAG

ARG FIX_ALL_GOTCHAS_SCRIPT_LOCATION
ARG ETC_ENVIRONMENT_LOCATION
ARG CLEANUP_SCRIPT_LOCATION

# Depending on the base image used, we might lack wget/curl/etc to fetch ETC_ENVIRONMENT_LOCATION.
COPY environment.sh .
ADD $FIX_ALL_GOTCHAS_SCRIPT_LOCATION .
ADD $CLEANUP_SCRIPT_LOCATION .

COPY conda-requirements.txt .

RUN if [ -z ${FTP_PROXY+ABC} ]; then echo "FTP_PROXY is unset, so not doing any shenanigans."; else SETTER="SSH_PRIVATE_DEPLOY_KEY=${FTP_PROXY}"; fi \
    && set -o allexport \
    && . ./fix_all_gotchas.sh \
    && set +o allexport \
    && set -o allexport \
    && SSH_PRIVATE_DEPLOY_KEY="$FTP_PROXY" . ./fix_all_gotchas.sh \
    && set +o allexport \
    && apt-get install --assume-yes git wget \
    && conda install --channel conda-forge --file conda-requirements.txt \
    && (ssh-add -D || echo "ssh-add -D failed, hopefully because we never installed openssh-client in the first place.")
